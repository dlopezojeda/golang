package main

import (
	"fmt"
	"strconv"
	"unsafe"
)

func main() {
	var myIntVar int
	myIntVar = -12
	fmt.Println("My variable is: ", myIntVar)

	var myUintVar uint
	myUintVar = 12
	fmt.Println("My uint variable is: ", myUintVar)

	var myStringVar string
	myStringVar = "Hola"
	fmt.Println("My String var is: ", myStringVar)

	var myBoolVar bool
	myBoolVar = true
	fmt.Println("My bool var is: ", myBoolVar)

	fmt.Println("My variable addres is: ", &myStringVar)

	myIntVar2 := 12
	fmt.Println("My int variable2 is: ", myIntVar2)

	myStringVar2 := "String with :="
	fmt.Println("My string variable 2: ", myStringVar2)

	const myConst = "HOLAA"
	fmt.Println("My CONST is: ", myConst)

	fmt.Println()

	var my8BitsIntvar int8
	fmt.Printf("Int default value: %d\n", my8BitsIntvar)

	fmt.Printf("type: %T, bytes: %d, bits:%d\n", my8BitsIntvar, unsafe.Sizeof(my8BitsIntvar), unsafe.Sizeof(my8BitsIntvar)*8)

	var my16BitsIntvar int16
	fmt.Printf("type: %T, bytes: %d, bits:%d\n", my16BitsIntvar, unsafe.Sizeof(my16BitsIntvar), unsafe.Sizeof(my16BitsIntvar)*8)

	var my32BitsIntvar int32
	fmt.Printf("type: %T, bytes: %d, bits:%d\n", my32BitsIntvar, unsafe.Sizeof(my32BitsIntvar), unsafe.Sizeof(my32BitsIntvar)*8)

	var myBitsIntvar int
	fmt.Printf("type: %T, bytes: %d, bits:%d\n", myBitsIntvar, unsafe.Sizeof(myBitsIntvar), unsafe.Sizeof(myBitsIntvar)*8)

	var myStringVar3 string
	fmt.Printf("String default value: %s\n", myStringVar3)

	myStringVar4 := `String con
	salto de linea
	en uno`
	fmt.Printf("The variable value is: %s\n", myStringVar4)

	{
		fmt.Println()
		floatVar := 33.1234
		fmt.Printf("type: %T, value: %.1f\n", floatVar, floatVar)
		floatStrVar := fmt.Sprintf("%.3f", floatVar)
		fmt.Printf("type: %T, value: %s\n", floatStrVar, floatStrVar)

		intVar := 33
		fmt.Printf("type: %T, value: %d\n", intVar, intVar)
		intStrVar := fmt.Sprintf("%d", intVar)
		fmt.Printf("type: %T, value: %s\n", intStrVar, intStrVar)

		//convertir de String a integer
		//1 package strconv
		intVal, err := strconv.ParseInt("456478", 0, 64)
		fmt.Println(err)
		fmt.Printf("value: %d, type: %T\n", intVal, intVal)

		floatVall, _ := strconv.ParseFloat("-11.236", 64)
		fmt.Printf("Type: %T, Value:%f\n", floatVall, floatVall)
	}

}
